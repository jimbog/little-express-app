var express = require('express');
var bodyParser = require('body-parser');
var mongo = require('mongoskin');

var db = mongo.db("mongodb://localhost:27017/blogger");
db.bind('posts');

var app = express();
app.use(bodyParser.urlencoded());

app.get('/', function(request, response){
  response.render('index.jade');
});

app.get('/posts', function(request, response){
  db.posts.find().toArray(function(err, result){
    console.log(result);
    response.render('posts-list.jade', {posts: result });
  });
});

app.get('/posts/:id', function(request, response){
  db.posts.findById(request.params.id, function(err, result){
    response.render('post-show.jade', {post: result });
  });
});

app.post('/posts', function(request, response){
  console.log(request.body);
  db.posts.insert(request.body, function(err, result){
    response.redirect("/posts");
  });
});

app.listen(3000);

